**Usage:** python ./quantize.py \<inputFile\> \<w\/h\> \<size\> \<canvas\> \<x\> \<y\>
- Converts input file to specified (hardcoded) palette
- Resizes to specified **w**(idth)/**h**(eight)
- Places on canvas on **x**, **y**
- Generates a list of required commands to paint

##### Do not use relative paths, all files must be placed in a single directory.



**Example:** python ./quantize.py image.png h 150 board_1-1.png 0 0

TODO:
- Add check for placed pixels