#!/usr/bin/env python3
from PIL import Image, ImageOps, ImageChops
import sys

if len(sys.argv) != 7:
    print("Usage: python ./quantize.py <inputFile> <w/h> <size> <canvas> <x> <y>")
    exit()

palettedata = [244, 123, 103,
               248, 165, 50,
               254, 231, 92,
               87, 242, 135,
               72, 183, 132,
               69, 221, 192,
               153, 170, 181,
               35, 39, 42 ,
               183, 194, 206,
               65, 135, 247,
               54, 57, 63 ,
               62, 112, 221,
               79, 93, 127,
               114, 137, 218,
               78, 93, 148,
               88, 101, 242,
               69, 79, 191,
               156, 132, 239,
               244, 127, 255,
               235, 69, 158,
               237, 66, 69,
               255, 255, 255,
               23, 161, 103,
               198, 59, 104,
               198,  59, 104,
               198,  59, 104,
               198,  59, 104,
               198,  59, 104,
               198,  59, 104,
               198,  59, 104,
               198,  59, 104,
               198,  59, 104,]
colors = {'244, 123, 103': 'brll',
          '248, 165, 50': 'hpsq',
          '254, 231, 92': 'yllw',
          '87, 242, 135': 'gren',
          '72, 183, 132': 'bhnt',
          '69, 221, 192': 'blnc',
          '153, 170, 181': 'grpl',
          '35, 39, 42': 'nqbl',
          '183, 194, 206': 'ntgr',
          '65, 135, 247': 'ptnr',
          '54, 57, 63': 'dgry',
          '62, 112, 221': 'devl',
          '79, 93, 127': 'ntbl',
          '114, 137, 218': 'lbpl',
          '78, 93, 148': 'ldbp',
          '88, 101, 242': 'blpl',
          '69, 79, 191': 'dbpl',
          '156, 132, 239': 'brvy',
          '244, 127, 255': 'bstp',
          '235, 69, 158': 'fchs',
          '237, 66, 69': 'dred',
          '255, 255, 255': 'whte',
          '23, 161, 103': 'ttsu',
          '198, 59, 104': 'ayna'}

palimage = Image.new('P', (16, 16))
palimage.putpalette(palettedata * 8)
input_image = Image.open(sys.argv[1]) # input file
m_size = int(sys.argv[3]) # max size
w = input_image.size[0]
h = input_image.size[1]
print(f"Input img: {w}x{h}")
if sys.argv[2] == 'w':
    ratio = m_size / w
    w = m_size
    h = int(h * float(ratio))
else:
    ratio = m_size / h
    w = int(w * float(ratio))
    h = m_size
print(f"Output img: {w}x{h}")
input_image = input_image.resize((w, h), 4)  # resampling filter Image.NEAREST (0), Image.LANCZOS (1), Image.BILINEAR (2), Image.BICUBIC (3), Image.BOX (4) or Image.HAMMING (5)

mask_image = Image.new("1", input_image.size, 1)
if input_image.mode == "RGBA":
    mask_pixels = mask_image.load()
    alpha_channel = input_image.load()
    for y in range(input_image.size[1]):
        for x in range(input_image.size[0]):
            if alpha_channel[x, y][3] == 0:
                mask_pixels[x, y] = 0
            else:
                mask_pixels[x, y] = 1
    input_image = input_image.convert("RGB")

new_image = input_image.quantize(colors=24, method=3, kmeans=0, palette=palimage, dither=0) # method – 0 = median cut 1 = maximum coverage 2 = fast octree 3 = libimagequant

canvas = Image.open(sys.argv[4])
canvas = canvas.crop((canvas.size[0]-1500, canvas.size[1]-1500, canvas.size[0], canvas.size[1]))
canvas = canvas.resize((500, 500), 0)
#canvas = Image.new("RGBA", (w,h))
canvas.paste(new_image, (int(sys.argv[5]), int(sys.argv[6])), mask_image)

# # TODO subtract diff / check already placed
# #diff = ImageChops.difference(canvas_cp, ImageChops.difference(canvas, map))
# #diff.show()
# #diff.save("overwrite.png")

map = Image.new("RGBA", canvas.size)
map.paste(new_image, (int(sys.argv[5]), int(sys.argv[6])), mask_image)
map = map.load()
count = 0
fName = f"blurpleCommands_{sys.argv[1]}.txt"
with open(fName, 'w') as f:
    for i in range(0, 500):
        for j in range(0, 500):
            if map[j, i][3] == 255:
                f.write(f"p/place {j + 1} {i + 1} {colors[str(map[j, i][:3])[1:-1]]}\n")
                count += 1
print(f"Pixels to place: {count}")
print(f"Commands saved in {fName}")

fName = f"blurpleCanvas_{sys.argv[1]}"
canvas.save(fName) #.save/show
print(f"Saved as {fName}")
